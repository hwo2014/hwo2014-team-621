# -*- coding: UTF-8 -*-

import json
import copy

from template import NoobBot

import assist

class Eracer (NoobBot):
    """Eracer bot for the Hello World Open 2014 competition
    https://helloworldopen.com

    E-racing team:
    Eero Penttilä
    eero.penttila@kotinet.com

    Eracer class expands the NoobBot class tamplate given to the competition.
    """

    def __init__(self, socket, name, key):
        NoobBot.__init__(self, socket, name, key)
        
        #updated on 'self.car_init':
        self.color = None

        #updated on 'track_init':
        self.track = None
        self.dimensions = None
        self.race_session = None
        self.cars_on_track = None

        #updated on 'on_car_positions':
        self.last_position = None
        self.current_position = {'tick': 0,  'angle': 0, 'piece_index': 0,
                'in_piece_distance': 0.0, 'start_lane': 0, 'end_lane': 0, 'lap': 0}
        self.max_angle = {'angle': 0, 'piece_index': 0, 'lap': 0}
        self.max_speed = {'speed': 0, 'piece_index': 0, 'lap': 0}
        self.target_speed = 1.0

        #debug
        self.print_counter = 0

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'yourCar': self.car_init,
            'gameInit': self.track_init,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
        }
        socket_file = self.socket.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()

    def car_init(self, data):
        self.color = data['color']
        print("Our car is {0}".format(self.color))
        self.ping()

    def track_init(self, data):
        self.track = data['race']['track']
        for car in data['race']['cars']:
            if car['id']['color'] == self.color:
                self.dimensions = car['dimensions']
                break
        self.cars_on_track = len(data['race']['cars'])
        self.race_session = data['race']['raceSession']
        if self.race_session.has_key('laps'):
            print("We are racing in {0} with {1} cars for {2} laps.".format(self.track['name'], 
                self.cars_on_track, self.race_session['laps']))
        elif self.race_session.has_key('durationMs'):
            print("We are qualifying in {0} with {1} cars for {2} sec.".format(self.track['name'], 
                self.cars_on_track, self.race_session['durationMs']/1000))
        self.ping()

    def on_car_positions(self, data):
        self.last_position = copy.deepcopy(self.current_position)
        for car in data:
            if car['id']['color'] == self.color:
                self.current_position = {
                        'tick': 0, 
                        'angle': car['angle'], 
                        'piece_index': car['piecePosition']['pieceIndex'], 
                        'in_piece_distance': car['piecePosition']['inPieceDistance'], 
                        'start_lane': car['piecePosition']['lane']['startLaneIndex'], 
                        'end_lane': car['piecePosition']['lane']['endLaneIndex'], 
                        'lap': car['piecePosition']['lap']
                        }
                break
        #self.current_position['tick'] = data['gameTick'] #The tick doesn't get passed with the data 
        #       -> figure out something...

        #record slip angle
        if abs(self.current_position['angle']) > self.max_angle['angle']:
            self.max_angle = {'angle': abs(self.current_position['angle']), 
                    'piece_index': self.current_position['piece_index'], 'lap': self.current_position['lap']}

        #speed since last tick
        if self.last_position['piece_index'] == self.current_position['piece_index']:
            current_speed = -self.last_position['in_piece_distance'] + self.current_position['in_piece_distance']
        else:
            current_speed = -self.last_position['in_piece_distance'] + \
                    assist.get_track_piece_length(
                        track = self.track, 
                        piece = self.last_position['piece_index'], 
                        lane = self.last_position['end_lane']) + \
                    self.current_position['in_piece_distance']

        #record max speed
        if current_speed > self.max_speed['speed']:
            self.max_speed = {'speed': current_speed, 
                    'piece_index': self.current_position['piece_index'], 'lap': self.current_position['lap']}

        #set target speed on start of each new track piece
        if self.last_position['piece_index'] != self.current_position['piece_index']:
            self.target_speed = assist.get_target_speed(track = self.track, piece = self.current_position['piece_index']+1, 
                    lane = self.current_position['start_lane'])

        #throttle to apply
        throttle = self.target_speed - (current_speed /10 - self.target_speed) * 5

        #debug
        '''
        if self.print_counter <= 0:
            self.print_counter = 10
            print("speed {0:.2f}, target {1:.2f}, throttle {2:.2f}, slip angle {3:.2f} in piece {4}:{5:.2f}".format(current_speed, self.target_speed, throttle, self.current_position['angle'], self.current_position['piece_index'], self.current_position['in_piece_distance']))
        else:
            self.print_counter -= 1
        '''

        #limit throttle if in near crash at exit of corner
        if abs(self.current_position['angle']) > 56 and throttle > 0.9:
            throttle = 0.9
            #print("angle {0:.2f} throttle 9".format(self.current_position['angle']))

        #make sure that throttle is in given limits
        if throttle > 1.0:
            throttle = 1.0
        elif throttle < 0:
            throttle = 0

        #send response
        self.throttle(throttle)

    def on_game_end(self, data):
        print("Race ended")
        print("Higest slip angle was {0:.2f}, in piece {1} in lap {2}".format(self.max_angle['angle'], 
                self.max_angle['piece_index'], self.max_angle['lap']))
        print("Higest speed was {0:.2f}, in piece {1} in lap {2}".format(self.max_speed['speed'], 
                self.max_speed['piece_index'], self.max_speed['lap']))
        self.ping()
