"""Some unit tests for the project

unittest cases:
assertEqual()
assertTrue()
assertRaises()
"""

import unittest

import assist

class assist_get_track_piece(unittest.TestCase):
    """Tests for assist.get_track_piece() -function
    """

    def setUp(self):
        self.track = {'pieces': [{'length': 100.0}, {'length': 100.0}, {'length': 100.0}, {'length': 100.0, 'switch': True}, {'angle': 45.0, 'radius': 100}, {'angle': 45.0, 'radius': 100}, {'angle': 45.0, 'radius': 100}, {'angle': 45.0, 'radius': 100}, {'angle': 22.5, 'switch': True, 'radius': 200}, {'length': 100.0}, {'length': 100.0}, {'angle': -22.5, 'radius': 200}, {'length': 100.0}, {'length': 100.0, 'switch': True}, {'angle': -45.0, 'radius': 100}, {'angle': -45.0, 'radius': 100}, {'angle': -45.0, 'radius':100}, {'angle': -45.0, 'radius': 100}, {'length': 100.0, 'switch': True}, {'angle': 45.0, 'radius': 100}, {'angle': 45.0, 'radius': 100}, {'angle': 45.0, 'radius': 100}, {'angle': 45.0, 'radius': 100}, {'angle': 22.5, 'radius': 200}, {'angle': -22.5, 'radius': 200}, {'length': 100.0, 'switch': True},{'angle': 45.0, 'radius': 100}, {'angle': 45.0, 'radius': 100}, {'length':62.0}, {'angle': -45.0, 'switch': True, 'radius': 100}, {'angle': -45.0, 'radius': 100}, {'angle': 45.0, 'radius': 100}, {'angle': 45.0, 'radius': 100}, {'angle': 45.0, 'radius': 100}, {'angle': 45.0, 'radius': 100}, {'length': 100.0, 'switch': True}, {'length': 100.0}, {'length': 100.0}, {'length': 100.0}, {'length': 90.0}], 
            'lanes': [{'index': 0, 'distanceFromCenter': -10}, {'index': 1, 'distanceFromCenter': 10}], 
            'id': 'keimola', 
            'startingPoint': {'position': {'y': -44.0, 'x': -300.0}, 'angle': 90.0}, 
            'name': 'Keimola'}

    def test_first_item(self):
        self.assertEqual(assist.get_track_piece(track = self.track, piece = 0), {'length': 100.0})

    def test_last_item(self):
        self.assertEqual(assist.get_track_piece(track = self.track, piece = len(self.track['pieces'])-1), {'length': 90.0})

    def test_last_plus_one(self):
        self.assertEqual(assist.get_track_piece(track = self.track, piece = len(self.track['pieces'])), {'length': 100.0})

    def test_no_track_passed(self):
        self.assertEqual(assist.get_track_piece(piece = 0), None)

    def test_negative_piece_index(self):
        self.assertEqual(assist.get_track_piece(track = self.track, piece = -1), None)

    def tearDown(self):
        pass

class assist_get_track_piece_length(unittest.TestCase):
    """Tests for assist.get_track_piece_length() -function
    """

    def setUp(self):
        self.track = {'pieces': [{'length': 100.0}, {'length': 100.0}, {'length': 100.0}, {'length': 100.0, 'switch': True}, {'angle': 45.0, 'radius': 100}, {'angle': 45.0, 'radius': 100}, {'angle': 45.0, 'radius': 100}, {'angle': 45.0, 'radius': 100}, {'angle': 22.5, 'switch': True, 'radius': 200}, {'length': 100.0}, {'length': 100.0}, {'angle': -22.5, 'radius': 200}, {'length': 100.0}, {'length': 100.0, 'switch': True}, {'angle': -45.0, 'radius': 100}, {'angle': -45.0, 'radius': 100}, {'angle': -45.0, 'radius':100}, {'angle': -45.0, 'radius': 100}, {'length': 100.0, 'switch': True}, {'angle': 45.0, 'radius': 100}, {'angle': 45.0, 'radius': 100}, {'angle': 45.0, 'radius': 100}, {'angle': 45.0, 'radius': 100}, {'angle': 22.5, 'radius': 200}, {'angle': -22.5, 'radius': 200}, {'length': 100.0, 'switch': True},{'angle': 45.0, 'radius': 100}, {'angle': 45.0, 'radius': 100}, {'length':62.0}, {'angle': -45.0, 'switch': True, 'radius': 100}, {'angle': -45.0, 'radius': 100}, {'angle': 45.0, 'radius': 100}, {'angle': 45.0, 'radius': 100}, {'angle': 45.0, 'radius': 100}, {'angle': 45.0, 'radius': 100}, {'length': 100.0, 'switch': True}, {'length': 100.0}, {'length': 100.0}, {'length': 100.0}, {'length': 90.0}], 
            'lanes': [{'index': 0, 'distanceFromCenter': -10}, {'index': 1, 'distanceFromCenter': 10}], 
            'id': 'keimola', 
            'startingPoint': {'position': {'y': -44.0, 'x': -300.0}, 'angle': 90.0}, 
            'name': 'Keimola'}

    def test_first_item(self):
        self.assertEqual(assist.get_track_piece_length(track = self.track, piece = 0), 100.0)

    def test_last_item(self):
        self.assertEqual(assist.get_track_piece_length(track = self.track, piece = len(self.track['pieces'])-1), 90.0)

    def test_last_plus_one(self):
        self.assertEqual(assist.get_track_piece_length(track = self.track, piece = len(self.track['pieces'])), 100.0)

    def test_lane1(self):
        self.assertEqual(assist.get_track_piece_length(track = self.track, piece = 0, lane = 1), 100.0)

    def test_corner_piece(self):
        self.assertEqual(assist.get_track_piece_length(track = self.track, piece = 7), 86.39379797371932)

    def test_another_corner_piece(self):
        self.assertEqual(assist.get_track_piece_length(track = self.track, piece = 8), 82.46680715673207)

    def test_corner_piece_lane1(self):
        self.assertEqual(assist.get_track_piece_length(track = self.track, piece = 7, lane = 1), 70.68583470577035)

    def test_another_corner_piece_lane1(self):
        self.assertEqual(assist.get_track_piece_length(track = self.track, piece = 8, lane = 1), 74.61282552275759)

    def test_negative_corner_piece(self):
        self.assertEqual(assist.get_track_piece_length(track = self.track, piece = 11), 74.61282552275759)

    def test_negative_corner_piece_lane1(self):
        self.assertEqual(assist.get_track_piece_length(track = self.track, piece = 11, lane = 1), 82.46680715673207)

    def test_no_track_passed(self):
        self.assertEqual(assist.get_track_piece_length(piece = 0), None)

    def test_negative_piece_index(self):
        self.assertEqual(assist.get_track_piece_length(track = self.track, piece = -1), None)

    def test_invalid_lane(self):
        self.assertEqual(assist.get_track_piece_length(track = self.track, lane = -1), None)

    def tearDown(self):
        pass

if __name__ == '__main__':
    unittest.main()
