import socket
import sys

from e_racer import Eracer

if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
        print("Running with default parameters")
        host, port, name, key = "testserver.helloworldopen.com", "8091", "E-racing", "9ojEQwnaSUDqAg"
    else:
        host, port, name, key = sys.argv[1:5]
    print("Connecting with parameters:")
    print("host={0}, port={1}, bot name={2}".format(host, port, name))
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, int(port)))
    bot = Eracer(s, name, key)
    bot.run()
