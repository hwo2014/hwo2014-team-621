"""Assist functions for the E-racing team bot
"""

import math

def get_track_piece(track = None, piece = 0):
    """return track piece #pieces from dictionary track
    warp aroun dict, if piece index > len
    return None, if no track inputted, or piece index is negative
    """
    if type(track) == dict and track.has_key('pieces') and piece >= 0:
        return track['pieces'][piece%len(track['pieces'])]
    else:
        print("get_track_piece: Error")
        return None

def get_track_piece_length(track = None, piece = 0, lane = 0):
    """return length of given track piece.
    return None, if track piece or line is not found
    """
    piece = get_track_piece(track = track, piece = piece)
    lane_distance = None
    if type(track) == dict and track.has_key('lanes'):
        for track_lane in track['lanes']:
            if track_lane['index'] == lane:
                lane_distance = track_lane['distanceFromCenter']
                break
    if type(piece) == dict and lane_distance != None:
        if piece.has_key('angle') and piece.has_key('radius'):
            #corner piece
            return 2 * math.pi * (piece['radius'] - lane_distance * 
                    piece['angle'] / abs(piece['angle'])) / 360 * abs(piece['angle'])
        else:
            #straight piece
            return piece['length']
    else:
        print("get_track_piece_length: Error")
        return None

def get_target_speed(track = None, piece = 0, lane = 0):
    """return rough estimation to what speed the corner can be driven
    This is still under developement, so no unittests.
    (this might be used as starting point of learning th max speed, not directly to say it)
    """
    cur_piece = get_track_piece(track = track, piece = piece)
    if type(cur_piece) == dict:
        if cur_piece.has_key('angle'):
            #we have corner peace
            length = get_track_piece_length(track = track, piece = piece, lane = lane)
            return 3.7 * length / abs(cur_piece['angle']) / 10.0
        else:
            #in case of straight, go full steam
            return 1.0
    else:
        print("get_target_speed: Error")
        return None
