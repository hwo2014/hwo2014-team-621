"""Another executable to crate/join to advanced test server
"""

import socket
import sys

from e_racer import Eracer

class Bot (Eracer):

    def join(self):
        msg_type = create + "Race"
        data = {"botId": {
                "name": self.name,
                "key": self.key
            },
                "trackName": trackName,
                "password": password,
                "carCount": int(carCount)
            }
        return self.msg(msg_type, data)

if __name__ == "__main__":
    if len(sys.argv) != 9:
        print("Usage: ./run host port botname botkey [crate|join] trackName password carCount")
        print("Running with default parameters")
        host, port, name, key, create, trackName, password, carCount = "hakkinen.helloworldopen.com", "8091", "E-racing", "9ojEQwnaSUDqAg", "create", "keimola", "testiajot", "1"
    else:
        host, port, name, key, create, trackName, password, carCount = sys.argv[1:9]
    print("Connecting with parameters:")
    print("host={0}, port={1}, bot name={2}".format(host, port, name))
    print("{0}: track={1}, password={2}, number of bots={3}".format(create, trackName, password, carCount))
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, int(port)))
    bot = Bot(s, name, key)
    bot.run()
